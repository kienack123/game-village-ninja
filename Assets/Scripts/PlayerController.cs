using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float maxSpeed;
    [SerializeField]
    private float jumpHeight;
    private bool facingRight = true;
    private bool isGround;
    [SerializeField]
    private float speedTele;
    [SerializeField] private new Rigidbody2D rigidbody;
    private Animator anim;

    // var for use skillDragon
    [SerializeField]
    private Transform AttackPoint;
    [SerializeField]
    private GameObject skillDragonRight;
    [SerializeField]
    private GameObject skillDragonLeft;
    private AudioSource footStep;
   
    private float attackRate = 2f;
    private float nextTimeAttack = 0f;



    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        footStep =GetComponent<AudioSource>();
    }
    protected void Update()
    {
        // Move
        float move = Input.GetAxis("Horizontal");
        anim.SetFloat("Speed", Mathf.Abs(move));
        rigidbody.velocity = new Vector2(move * maxSpeed, rigidbody.velocity.y);
        if (move > 0 && !facingRight)
        {
            Flip();
        }
        else if (move < 0 && facingRight)
        {
            Flip();
        }

        // jump

        if (Input.GetKey(KeyCode.Space))
        {
            if (isGround)
            {
                isGround = false;
                rigidbody.velocity = new Vector2(rigidbody.velocity.x, jumpHeight);
                anim.SetBool("isJump", true);

            }
        }



        // Teleport

        if (Input.GetKey(KeyCode.E))
        {

            rigidbody.AddForce(Vector2.right * speedTele, ForceMode2D.Impulse);
            anim.SetBool("isAvoidFront", true);

        }
        else if (Input.GetKeyUp(KeyCode.E))
        {

            anim.SetBool("isAvoidFront", false);

        }

        if (Input.GetKey(KeyCode.Q))
        {

            rigidbody.AddForce(Vector2.left * speedTele, ForceMode2D.Impulse);
            anim.SetBool("isAvoidBack", true);
        }
        else if (Input.GetKeyUp(KeyCode.Q))
        {


            anim.SetBool("isAvoidBack", false);

        }



        // Anim 1 

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            anim.SetBool("isAnim1", true);

        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            anim.SetBool("isAnim1", false);

        }

        // HEAL

        if (Input.GetKeyDown(KeyCode.W))
        {
            anim.SetBool("isHeal", true);

        }
        else if (Input.GetKeyUp(KeyCode.W))
        {
            anim.SetBool("isHeal", false);

        }


        // Attack normal
        if (Time.time > nextTimeAttack)
        {
            if (Input.GetAxisRaw("Fire1") > 0)
            {
                anim.SetTrigger("Attack");
                nextTimeAttack = Time.time + 1f / attackRate;
               
            }
        }


        // Use Skill
        if (Input.GetAxisRaw("Fire2") > 0)
        {
            anim.SetTrigger("Attack");

            StartCoroutine(SpawnSkill());
        }


        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            anim.SetBool("BG", true);

        }
        else if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            anim.SetBool("BG", false);

        }

        // Check Jumping or Falling
        anim.SetFloat("yVelocity", rigidbody.velocity.y);
    }

    // Fun Flip Face
    protected private void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }



    protected void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Ground")
        {
            isGround = true;
            anim.SetBool("isJump", !isGround);
        }
    }

    private void UseSkillDragon()
    {
        if(Time.time > nextTimeAttack)
        { 
            
            if (facingRight)
            {
                Instantiate(skillDragonRight, AttackPoint.position, Quaternion.Euler(new Vector3 (0 , 0 ,0)));
                
            }
            else if(!facingRight)
            {
                Instantiate(skillDragonLeft, AttackPoint.position, Quaternion.Euler(new Vector3(0 , 0 , 0)));
                
            }
            nextTimeAttack = Time.time + 1f / attackRate;


        }
        
    }


    IEnumerator SpawnSkill()
    {
        yield return new WaitForSeconds(0.5f);
        UseSkillDragon();
    }

    protected void FootStep()
    {
        footStep.Play();
    }
}
