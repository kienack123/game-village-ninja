using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillDragonController : MonoBehaviour
{
    public float speedFly;
    private new Rigidbody2D rigidbody;
    public GameObject hitEffect;
    // Start is called before the first frame update
    void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        if(transform.localScale.x > 0  )
        {
            rigidbody.AddForce(Vector2.right * speedFly, ForceMode2D.Impulse);
        }
        else 
        {
            rigidbody.AddForce(Vector2.left * speedFly, ForceMode2D.Impulse);
        }
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
            Destroy(effect, 0.7f);
            Destroy(gameObject);
        }
    }
    
}
